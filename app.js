
var fs = require('fs');
var csv = require('csv-parser');
const pool = require('./pgdb');
const moment = require('moment');
var squel = require("squel");

pool.connect(function(err){
    if(err)
    {
        console.log(err);
    }
});

let counter = 0; 

// let header = [];
// let data = [];


let csvStream = function(filename){
    return new Promise((resolve)=>{
        fs.createReadStream(filename,{headers:true})
        .pipe(csv())
        .on("data", function(record){
            csv().pause();
            //if(counter < 1)
            //{
                var query
                let item_name = record.item_name;
                let purchase_date
                let sales_date
                filename === "sales_items.csv" ? sales_date = moment(record.sales_date,"DD-MM-YYYY").format("YYYY-MM-DD") : purchase_date = moment(record.purchase_date,"DD-MM-YYYY").format("YYYY-MM-DD");
                let qty = record.qty;
                let unit = record.unit;
                let price = record.price;

                filename ==="sales_items.csv" ?
                query = squel.insert().into("sales").setFields({ item_name: item_name, sales_date: sales_date, qty:qty, unit:unit, price:price }).toString() :
                query = squel.insert().into("stock").setFields({ item_name: item_name, purchase_date: purchase_date, qty:qty, unit:unit, price:price }).toString();
                //console.log(query)
                pool.query(query, function(err){
                    if(err)
                    {
                        console.log(err);
                    }
                });
                ++counter;
            //}
            csv().resume();
        }).on("end", function(){
            console.log("Job is done!");
            resolve()
        }).on("error", function(err){
            console.log(err);
        });
    })
}

async function f(){
    await csvStream("stock_item.csv");
    await csvStream("sales_items.csv");
}
f()

    
